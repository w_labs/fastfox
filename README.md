# fastfox
[about:config](https://support.mozilla.org/en-US/kb/about-config-editor-firefox) and [policies.json](https://support.mozilla.org/en-US/kb/customizing-firefox-using-policiesjson) tweaks to enhance [Mozilla Firefox](https://www.mozilla.org/en-US/firefox/new/).

## How to use?

- Open `about:support` at Firefox or any Firefox-folk. Copy `Profile Folder` location.
- Clean `pref.js` with `utils/prefsCleaner.sh` for Linux/MacOS or `utils/prefsCleaner.bat` for Windows.
- Move `user.js` and `chrome` to `Profile Folder` directory.
- Move `distribution` folder to Firefox installation location. eg: `C:\Program Files\Mozilla Firefox\`
- Enjoy.


## For Linux users

### Fix `Open folder` in Firefox

Sometimes Firefox will not follow your `mimeapps.list`. Here is a guide how you can fix that, example is for `Dolphin`:

- Run those command to set default file manager

```shell
xdg-mime default org.kde.dolphin.desktop inode/directory
xdg-mime default org.kde.dolphin.desktop x-directory/normal
```

- Use `user-linux.js` file in repo, remember rename it to `user.js` before using.

## Credits

- [Betterfox](https://github.com/yokoffing/Betterfox)
- [arkenfox/user.js](https://github.com/arkenfox/user.js)
