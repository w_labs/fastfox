/****************************************************************************
 * SECTION: LINUX                                                           *
 ****************************************************************************/

user_pref("widget.use-xdg-desktop-portal.file-picker", 1);
user_pref("widget.use-xdg-desktop-portal.mime-handler", 1);
user_pref("media.ffmpeg.vaapi.enabled", true);
