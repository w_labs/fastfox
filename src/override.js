/****************************************************************************
 * SECTION: OVERRIDE                                                        *
 ****************************************************************************/

user_pref("ui.prefersReducedMotion", 1);
user_pref("network.http.http3.cc_algorithm", 0);
user_pref("browser.cache.jsbc_compression_level", 2);
user_pref("dom.script_loader.bytecode_cache.strategy", -1);
user_pref("javascript.options.mem.gc_balanced_heap_limits", true);
user_pref("privacy.sanitize.useOldClearHistoryDialog", true);
user_pref("browser.contentblocking.category", "standard");
