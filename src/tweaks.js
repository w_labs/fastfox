/****************************************************************************
 * SECTION: TWEAKS                                                          *
 ****************************************************************************/

/** INSTANT STARTUP ***/
user_pref("browser.startup.preXulSkeletonUI", false);

/** SNAPPIER TOOLTIPS */
user_pref("ui.tooltipDelay", 60);
user_pref("ui.submenuDelay", 0);
user_pref("browser.overlink-delay", 10);

/** DISABLE ACCESSIBILITY  ***/
user_pref("accessibility.force_disabled", 1);

/** DISABLE TASKBAR PREVIEW ***/
user_pref("browser.taskbar.lists.enabled", false);
user_pref("browser.taskbar.lists.frequent.enabled", false);
user_pref("browser.taskbar.lists.maxListItemCount", 0);
user_pref("browser.taskbar.lists.tasks.enabled", false);
user_pref("browser.taskbar.previews.max", 0);

/** RENDERING SPEED ***/
user_pref("nglayout.initialpaint.delay", 2000);
user_pref("nglayout.initialpaint.delay_in_oopif", 2000);
user_pref("content.notify.backoffcount", 0);
user_pref("content.notify.ontimer", true);

/** DNS ***/
user_pref("network.trr.uri", "https://ultralow.dns.nextdns.io");
user_pref("network.trr.custom_uri", "https://ultralow.dns.nextdns.io");
user_pref("network.trr.mode", 2);
user_pref("network.trr.disable-ECS", false);
user_pref("security.tls.enable_kyber", true);

/** RCWN ***/
user_pref("network.http.rcwn.enabled", false);

/** RAM CACHE ***/
user_pref("browser.cache.disk.enable", false);
user_pref("browser.cache.memory.enable", true);
user_pref("browser.cache.memory.capacity", 524288);
user_pref("browser.cache.memory.max_entry_size", 512000);

/** DISABLE FASTBACK ***/
user_pref("fission.bfcacheInParent", false);
user_pref("fission.disableSessionHistoryInParent", true);
user_pref("browser.sessionhistory.max_total_viewers", 0);

/** FIX FACEBOOK TIME ***/
user_pref("dom.textMetrics.fontBoundingBox.enabled", true);

/**  PREF: disable EcoQoS [WINDOWS]  */
user_pref("dom.ipc.processPriorityManager.backgroundUsesEcoQoS", false);

/** SCROLLBAR ***/

/*
0 - Default OS style
1 - MacOS style
2 - GTK style
3 - Google Android style
4 - Windows 10 style
5 - Windows 11 style
 */

user_pref("widget.non-native-theme.scrollbar.style", 2);

/****************************************************************************
 * END: TWEAKS                                                              *
 ****************************************************************************/
